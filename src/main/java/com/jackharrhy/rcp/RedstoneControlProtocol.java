package com.jackharrhy.rcp;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.Scanner;

import org.bukkit.command.BlockCommandSender;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.plugin.java.JavaPlugin;

public final class RedstoneControlProtocol extends JavaPlugin {

    @Override
    public void onEnable() {
        getLogger().info(getDescription().getName() + " has been enabled");
    }

    @Override
    public void onDisable() {
        getLogger().info(getDescription().getName() + " has been disabled");
    }

    private void makeRequest(CommandSender sender, String[] args) {
        if ((sender instanceof BlockCommandSender || sender.isOp()) && args.length != 0) {
            String queryParams = "?unsafeUrlSecret=" + getConfig().get("unsafeUrlSecret");

            if (args.length > 1) {
                queryParams += "&";

                for (int i = 1; i < args.length; i++) {
                    String arg = args[i];
                    if (i % 2 == 1) {
                        queryParams += arg;
                    } else {
                        queryParams += "=" + arg + "&";
                    }
                }

                StringBuilder sb = new StringBuilder(queryParams);
                sb.deleteCharAt(queryParams.length() - 1);
                queryParams = sb.toString();
            }

            String website = args[0] + queryParams;

            getLogger().info("pingit: " + website);

            try {
                URL url = new URL(website);
                URLConnection con = url.openConnection();
                InputStream connectionStream = con.getInputStream();

                Scanner in = new Scanner(connectionStream);

                while (in.hasNextLine()) {
                    String line = in.nextLine();
                }
                in.close();
            } catch (IOException e) {
                getLogger().info("pingit ran into an error! :(");
                if ((boolean) getConfig().get("debug")) e.printStackTrace();
            }
        }
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        switch (label.toLowerCase()) {
            case "pingit":
                makeRequest(sender, args);
                break;
        }
        return true;
    }
}
